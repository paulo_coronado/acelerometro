package com.example.sensores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    //1. Crear un objeto de la clase SensorManager
    SensorManager miSensorManager;
    //2. Crear un objeto de la clase Sensor
    Sensor miSensor;

    TextView txtAceleracion;

    int puntos=0;

    GraphView grafico;

    LineGraphSeries<DataPoint> series;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        txtAceleracion=findViewById(R.id.txtAceleracion);

        grafico=findViewById(R.id.grafico);

        grafico.getViewport().setXAxisBoundsManual(true);


        series=new LineGraphSeries<DataPoint>(new DataPoint[]{});

        //Agregar la serie a nuestro gráfico
        grafico.addSeries(series);

        //Cambiar color de las series
        series.setColor(getResources().getColor(R.color.black));


        //Inicializar el gestor de sensores
        miSensorManager= (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        //List<Sensor> misSensores=miSensorManager.getSensorList(Sensor.TYPE_ALL);
        miSensor=miSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        //Registrar el Listener
        miSensorManager.registerListener(this, miSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        int x= (int) sensorEvent.values[0];
        int y= (int) sensorEvent.values[1];
        int z= (int) sensorEvent.values[2];

        // Calcular distancia entre puntos
        double aceleracionTotal;
        aceleracionTotal= Math.sqrt(Math.pow(x,2) + Math.pow(y,2)+ Math.pow(z,2));
        txtAceleracion.setText("Aceleración Total: " + aceleracionTotal);

        //Muestre el punto en el gráfico
        DataPoint miPunto;
        miPunto= new DataPoint(series.getHighestValueX()+1, aceleracionTotal);
        puntos++;

        series.appendData(miPunto, true, puntos);

        grafico.getViewport().setMinX(puntos-150);
        grafico.getViewport().setMaxX(puntos);

        if (puntos >1000){
            puntos=0;
            series.resetData(new DataPoint[]{});
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflador=getMenuInflater();
        inflador.inflate(R.menu.main_menu,menu);
        return true;
    }
}